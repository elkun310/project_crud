<?php

namespace Tests\Unit\Models;

use App\Models\Task;
use App\Models\Status;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaskTest extends TestCase
{
    protected $task;

    public function setUp(): void
    {
        parent::setUp();
        $this->task = factory(Task::class)->create();
    }

    /** @test */
    public function a_task_belong_to_a_status()
    {
        $this->assertInstanceOf(Status::class, $this->task->status);
    }

}
