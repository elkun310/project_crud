<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comment', function (Blueprint $table) {
            $table->dropForeign(['idUser']);
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade')->change();
            $table->dropForeign(['idNew']);
            $table->foreign('idNew')->references('id')->on('news')->onDelete('cascade')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comment', function (Blueprint $table) {
            $table->foreign('idUser')->references('id')->on('users')->change();
            $table->foreign('idNew')->references('id')->on('news')->change();
        });
    }
}
