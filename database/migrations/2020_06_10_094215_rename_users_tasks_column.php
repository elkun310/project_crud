<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameUsersTasksColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('user_task', 'users_tasks');
        Schema::table('users_tasks', function(Blueprint $table) {
            $table->renameColumn('userid', 'user_id');
            $table->renameColumn('taskid', 'task_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_tasks', function(Blueprint $table) {
            $table->renameColumn('user_id', 'userid');
            $table->renameColumn('task_id', 'taskid');
        });
    }
}
