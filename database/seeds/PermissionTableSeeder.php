<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Permission::class, 4)->create();
        $permissions = Permission::pluck('id')->toArray();
        $role = Role::first();
        $role->permissions()->attach($permissions);
    }
}
