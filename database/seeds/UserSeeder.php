<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            ['id'=>1,'name'=>'administrator','email'=>'admin@gmail.com','password'=>bcrypt('123456'),'address'=>'Thường tín','phone'=>'0356653301','role'=>1],
            ['id'=>2,'name'=>'Nguyễn Thế Vũ','email'=>'zimpro@gmail.com','password'=>bcrypt('123456'),'address'=>'Bắc giang','phone'=>'0356654487','role'=>2],
            ['id'=>3,'name'=>'Nguyễn Quốc Hà','email'=>'nguyenha9772@gmail.com','password'=>bcrypt('123456'),'address'=>'Hà Nội','phone'=>'0389938822','role'=>1],
        ]);
    }
}
