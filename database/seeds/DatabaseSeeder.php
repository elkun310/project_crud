<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(TaskSeeder::class);
        $this->call(UserTaskSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(TypesSeeder::class);
        $this->call(NewsSeeder::class);
    }
}
