<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->delete();
        $content = array(
            'Bài viết rất hay',
            'Tôi rất thích bài viết này',
            'Bài viết này tạm ổn',
            'Hay quá trời',
            'Tôi sẽ học thèo bài viết này',
            'Bài viết này chưa được hay lắm',
            'Ý kiến của tôi khác so với bài này',
            'Bài viết này được',
            'Không thích bài viết này',
            'Tôi chưa có ý kiến gì'
        );

        for($i=1;$i<=22;$i++)
        {
            DB::table('comments')->insert(
                [
                    'user_id' => rand(1,4),
                    'new_id' => rand(1,22),
                    'content' => $content[rand(0,9)],
                    'created_at' => new DateTime()
                ]
            );
        }
    }
}
