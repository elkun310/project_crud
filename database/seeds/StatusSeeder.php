<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->delete();
        DB::table('status')->insert([
            [
                'id'    =>  1,
                'name'  =>  'Chưa bắt đầu'
            ],
            [
                'id'    =>  2,
                'name'  =>  'Đang thực hiện',
            ],
            [
                'id'    =>  3,
                'name'  =>  'Đang kiểm tra'
            ],
            [
                'id'    =>  4,
                'name'  => 'Chờ phản hồi',
            ],
            [
                'id'    =>  5,
                'name'  =>  'Đã hoàn thành'
            ]
        ]);
    }
}
