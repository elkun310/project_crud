<?php

/** @var Factory $factory */

use App\Models\Role;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Role::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['superAdmin', 'admin', 'employee']),
        'description' => $faker->paragraph,
    ];
});
