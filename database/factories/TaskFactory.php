<?php

/** @var Factory $factory */

use App\Models\Task;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle,
        'content' => $faker->paragraph,
        'status_id' => '1'
    ];
});
