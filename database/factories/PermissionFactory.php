<?php

/** @var Factory $factory */

use App\Models\Permission;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Permission::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(['list-user', 'add-user', 'edit-user', 'delete-user']),
        'description' => $faker->paragraph
    ];

});
