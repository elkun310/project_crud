<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;

trait HandleUploadImage
{
    public function storeUploadImage(UploadedFile $requestFile, $imagePath)
    {
        $imageName = time() . '-' . $requestFile->getClientOriginalName();
        $requestFile->move($imagePath, $imageName);
        return $imageName;
    }

    public function deleteUploadImage($imageName, $imagePath)
    {
        if (file_exists($imagePath . '/' . $imageName)) {
            if ($imageName != "user.png" & $imageName != "") {
                unlink($imagePath . '/' . $imageName);
            }
        }
    }

    public function updateUploadImage(UploadedFile $requestFile, $imagePath, $oldImage)
    {
        $imageName = $this->storeUploadImage($requestFile, $imagePath);
        $this->deleteUploadImage($oldImage, $imagePath);
        return $imageName;
    }
}
