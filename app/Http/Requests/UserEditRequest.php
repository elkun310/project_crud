<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required',
            'email'     =>  'required|email|unique:users,email,'.$this->hidden_id.',id',
            'image'     =>  'required|mimes:jpg,jpeg,png,bmp,tiff|max:4096',
            'address'   =>  'required',
            'phone'     =>  'required|numeric',
            'role'      =>  'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required'      =>  'Bạn chưa điền tên',
            'email.required'     =>  'Bạn chưa điền email',
            'email.email'        =>  'Email chưa đúng định dạng',
            'email.unique'       =>  'Email đã có trong hệ thống',
            'image.required'     =>  'Bạn chưa chọn ảnh',
            'image.mimes'        =>  'Ảnh không đúng định dạng',
            'image.max'          =>  'Dung lượng tối đa 4MB',
            'address.required'   =>  'Bạn chưa điền địa chỉ',
            'phone.required'     =>  'Bạn chưa điền số điện thoại',
            'phone.numeric'      =>  'Số điện thoại không đúng định dạng',
            'role.required'      =>  'Bạn chưa chọn quyền',
        ];
    }
}
