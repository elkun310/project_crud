<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|unique:categories,name,'.$this->hidden_id.',id'
        ];
    }
    public function messages()
    {
        return [
            'name.required'  =>  'Bạn chưa nhập tiêu đề',
            'name.unique'    =>  'Tiêu đề không được trùng'
        ];
    }
}
