<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth()->user()->hasRole($role)) {
            return $next($request);
        }
        return redirect('admin')->with('error', 'Bạn không được cấp quyền');
    }
}
