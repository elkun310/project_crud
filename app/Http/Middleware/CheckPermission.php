<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (Auth()->user()->hasPermission($permission)) {
            return $next($request);
        }
        return redirect('admin')->with('error', 'Bạn không được cấp quyền');

    }
}
