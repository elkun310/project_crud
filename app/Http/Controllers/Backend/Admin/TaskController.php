<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use App\Models\Status;
use App\Models\Task;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\View\View;
use App\Http\Requests\{TaskAddRequest, TaskEditRequest};
use Illuminate\Support\Facades\Auth;
use Throwable;

class TaskController extends Controller
{
    protected $user;
    protected $task;
    protected $status;

    /**
     * TaskController constructor.
     * @param Task $task
     * @param User $user
     * @param Status $status
     */
    public function __construct(Task $task, User $user, Status $status)
    {
        $this->task = $task;
        $this->user = $user;
        $this->status = $status;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $users = $this->user->getAll();
        $userId = Auth::user()->id;
        $tasks = $this->user->getTaskByUser($userId, 5);
        $status = $this->status->all();
        return view('backend.task.index', compact('users', 'tasks', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskAddRequest $request
     * @return array|string
     * @throws Throwable
     */
    public function store(TaskAddRequest $request)
    {
        $requestData = $request->all();
        $task = $this->task->saveTask($requestData);
        return response()->json([
            'urlTask' => route('tasks.show', $task->id),
            'message' => 'Thêm công việc thành công',
            'data' => $this->reloadTask()
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $task = $this->task->findOrFail($id);
        return view('backend.task.content', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $task = $this->task->findOrFail($id);
        $status = $this->status->all();
        $users = $this->user->getAll();
        return view('backend.task.edit', compact('users', 'task', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskEditRequest $request
     * @param int $id
     * @return array|string
     * @throws Throwable
     */
    public function update(TaskEditRequest $request, $id)
    {
        $requestData = $request->all();
        $task = $this->task->updateTask($requestData, $id);
        return response()->json([
            'id' => $task->id,
            'message' => 'Cập nhật công việc thành công',
            'data' => $this->reloadTask(),
            'urlTask' => route('tasks.show', $task->id)
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function updateContent(Request $request, $id)
    {
        $this->task->updateContent($request->all(), $id);
        return response()->json([
            'message' => 'Cập nhật thành công',
            'data' => $this->reloadTask()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array|string
     * @throws Throwable
     */
    public function destroy($id)
    {
        $task = $this->task->deleteTask($id);
        return response()->json([
            'message' => 'Xóa thành công',
            'id' => $task->id
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|string
     * @throws Throwable
     */
    public function changeStatus(Request $request, $id)
    {
        $requestData = $request->all();
        $this->task->changeStatus($requestData, $id);
        return response()->json([
            'message' => 'Cập nhật trạng thái thành công',
            'data' => $this->reloadTask()
        ]);
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws Throwable
     */
    public function fetchData(Request $request)
    {
        $users = $this->user->getAll();
        $status = $this->status->all();
        $txtSearch = str_replace(" ", "%", $request->get('txtSearch'));
        $tasks = $this->task->getTaskByQuery($txtSearch, 5);
        if ($tasks->count() > 0) {
            return view('backend.task.data_body', compact('users', 'status', 'tasks'))->render();
        } else {
            return "Không tìm thấy kết quả";
        }
    }

    /**
     * @return Application|Factory|View
     */
    public function doChart()
    {
        return view('backend.task.chart');
    }

    /**
     * @return array|string
     * @throws Throwable
     */
    public function reloadTask()
    {
        $users = $this->user->getAll();
        $userId = Auth::user()->id;
        $tasks = $this->user->getTaskByUser($userId, 5);
        $status = $this->status->all();
        return view('backend.task.data_body', compact('users', 'tasks', 'status'))->render();
    }
}
