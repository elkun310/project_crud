<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\{User, Role};
use Illuminate\View\View;
use Throwable;
use App\Http\Requests\{UserAddRequest, UserEditRequest};
use App\Traits\HandleUploadImage;

class UserController extends Controller
{
    use HandleUploadImage;

    protected $user;
    protected $role;
    protected $uploadPath = 'backend/images/users';

    /**
     * UserController constructor.
     * @param User $user
     * @param Role $role
     */
    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $users = $this->user->getPaginate(5);
        $roles = $this->role->all();
        return view('backend.user.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserAddRequest $request
     * @return array|string
     * @throws Throwable
     */
    public function store(UserAddRequest $request)
    {
        $requestData = $request->all();
        $requestData['image'] = $this->storeUploadImage($requestData['image'], $this->uploadPath);
        $this->user->store($requestData);
        return response()->json([
            'message' => 'Thêm thành công',
            'data' => $this->reloadUser()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $user = $this->user->findOrFail($id);
        $roles = $this->role->all();
        $roleByUser = $this->user->getRoleByUser($id);
        return view('backend.user.edit', compact('user', 'roles', 'roleByUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserEditRequest $request
     * @param int $id
     * @return array|string
     * @throws Throwable
     */
    public function update(UserEditRequest $request, $id)
    {
        $requestData = $request->all();
        $user = $this->user->findOrFail($id);
        $requestData['image'] = $this->updateUploadImage($requestData['image'], $this->uploadPath, $user->image);
        $this->user->updateUser($requestData, $id);
        return response()->json([
            'message' => 'Cập nhật thành công',
            'data' => $this->reloadUser()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array|string
     * @throws Throwable
     */
    public function destroy($id)
    {
        $user = $this->user->findOrFail($id);
        $this->deleteUploadImage($user->image, $this->uploadPath);
        $user = $this->user->deleteUser($id);
        return response()->json([
            'message' => 'Xóa thành công',
            'id' => $user->id
        ]);
    }

    /**
     * @param Request $request
     * @return array|string
     * @throws Throwable
     */
    public function fetchData(Request $request)
    {
        $txtSearch = $request->get('txtSearch');
        $users = $this->user->searchUser($txtSearch, 5);
        if ($users->count() > 0) {
            return view('backend.user.data_body', compact('users', 'txtSearch'))->render();
        } else {
            return "Không tìm thấy kết quả";
        }
    }

    /**
     * @return array|string
     * @throws Throwable
     */
    public function reloadUser()
    {
        $users = $this->user->getPaginate(5);
        return view('backend.user.data_body', compact('users'))->render();
    }
}
