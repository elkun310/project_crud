<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\{CategoryAddRequest, CategoryEditRequest};
use Illuminate\View\View;

class CategoryController extends Controller
{
    protected $category;

    /**
     * CategoryController constructor.
     * @param $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = $this->category->getPaginate(6);
        return view('backend.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryAddRequest $request
     * @return JsonResponse
     */
    public function store(CategoryAddRequest $request)
    {
        $requestData = $request->all();
        $this->category->saveCategory($requestData);
        return response()->json([
            'message' => 'Thêm danh mục thành công',
            'data' => $this->reloadCategory()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $category = $this->category->findOrFail($id);
        return view('backend.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryEditRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(CategoryEditRequest $request, $id)
    {
        $requestData = $request->all();
        $this->category->updateCategory($requestData, $id);
        return response()->json([
            'message' => 'Sửa danh mục thành công',
            'data' => $this->reloadCategory()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array|string
     */
    public function destroy($id)
    {
        $category = $this->category->deleteCategory($id);
        return response()->json([
            'message' => 'Xóa thành công',
            'id' => $category->id
        ]);
    }

    public function fetchData(Request $request)
    {
        $categories = $this->category->getPaginate(6);
        return view('backend.category.data_body', compact('categories'))->render();
    }

    public function reloadCategory()
    {
        $categories = $this->category->getPaginate(6);
        return view('backend.category.data_body', compact('categories'))->render();
    }
}
