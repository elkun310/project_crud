<?php

namespace App\Http\Controllers\Backend\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\Response;
use App\Http\Requests\{UserAddRequest,UserEditRequest};

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $users = User::all();
        return response()->json([
            'status' => '200',
            'data'   => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(UserAddRequest $request)
    {
        $request_data = $request->all();
        $request_data['password'] = bcrypt($request->input['password']);
        return User::create($request_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findorFail($id);
        return response()->json([
                'status'    =>  200,
                'message'   =>  'success',
                'data'      =>  $user
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findorfail($id);
        $request_data = $request->all();
        if(isset($request_data['password'])){
            $request_data['password'] = ($request_data['password']=="")?$user->password:bcrypt($request->input('password'));
        }
        $user->update($request_data);
        return response()->json([
            'status'    => 200,
            'message'   => 'Update user successfully',
            'data'      =>  $user
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response()->json([
            'status'    =>  204,
            'message'   =>  'Delete user successfully'
        ]);
    }
}

