<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = "tasks";

    protected $fillable = [
        'name',
        'content',
        'status_id'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_task');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function getAll()
    {
        return $this->latest()->get();
    }

    public function getPaginate($paginateNumber)
    {
        return $this->latest()->paginate($paginateNumber);
    }

    public function getTaskByQuery($txtSearch, $paginateNumber)
    {
        return $this->where('id', 'like', '%' . $txtSearch . '%')
            ->orWhere('name', 'like', '%' . $txtSearch . '%')
            ->orWhere('status_id', 'like', '%' . $txtSearch . '%')
            ->latest()
            ->with('users')
            ->with('status')
            ->paginate($paginateNumber);
    }

    public function saveTask(array $requestData)
    {
        $task = $this->create($requestData);
        $task->users()->attach($requestData['user']);
        return $task;
    }

    public function updateTask(array $requestData, $id)
    {
        $task = $this->findOrFail($id);
        $task->update($requestData);
        $task->users()->sync($requestData['user']);
        return $task;
    }

    public function deleteTask($id)
    {
        $task = $this->findOrFail($id);
        $task->users()->detach();
        $task->delete();
        return $task;
    }

    public function changeStatus(array $requestData, $id)
    {
        $task = $this->findOrFail($id);
        $task->update($requestData);
        return $task;
    }

    public function updateContent(array $requestData, $id)
    {
        $task = $this->findOrFail($id);
        $task->content = $requestData['content'];
        $task->save();
        return $task;
    }
}
