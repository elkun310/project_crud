<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = "types";

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
