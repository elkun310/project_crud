<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $fillable = [
        'name',
        'slug'
    ];

    public function types()
    {
        return $this->hasMany(Type::class, 'category_id', 'id');
    }

    public function getAll()
    {
        return $this->latest('id')->get();
    }

    public function getPaginate($paginateNumber)
    {
        return $this->latest('id')->paginate($paginateNumber);
    }

    public function saveCategory($requestData)
    {
        $requestData['slug'] = str_slug($requestData['name']);
        return $this->create($requestData);
    }

    public function updateCategory($requestData, $id)
    {
        $category = $this->findOrFail($id);
        $requestData['slug'] = str_slug($requestData['name']);
        $category->update($requestData);
        return $category;
    }

    public function deleteCategory($id)
    {
        $category = $this->findOrFail($id);
        $category->delete();
        return $category;
    }
}
