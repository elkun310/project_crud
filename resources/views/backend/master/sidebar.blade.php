<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/admin" class="site_title"><i class="fa fa-paw"></i> <span>CRUD ACDM</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="images/users/@if(Auth::check()){{Auth::user()->image}}@endif" alt="..."
                     class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Xin chào,</span>
                <h2>@if(Auth::check())
                        {{Auth::user()->name}}
                    @endif</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Tổng quan</h3>
                <ul class="nav side-menu">
                    <li><a href="/admin"><i class="fa fa-home"></i> Trang chủ</a>
                    </li>
                    @if(Auth::user()->hasRole('admin|superAdmin'))
                        <li><a href="{{route('users.index')}}"><i class="fa fa-user"></i> Nhân viên </a>
                        </li>
                    @endif
                    <li><a href="{{route('tasks.index')}}"><i class="fa fa-tasks"></i>Công việc</a>
                    </li>
                    <li><a href="{{route('categories.index')}}"><i class="fa fa-table"></i> Danh mục </a>
                    </li>
                    <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="chartjs.html">Chart JS</a></li>
                            <li><a href="chartjs2.html">Chart JS2</a></li>
                            <li><a href="morisjs.html">Moris JS</a></li>
                            <li><a href="echarts.html">ECharts</a></li>
                            <li><a href="other_charts.html">Other Charts</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>
