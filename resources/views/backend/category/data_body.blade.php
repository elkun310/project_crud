@foreach ($categories as $category)
    <tr class="odd gradeX" id="category{{$category->id}}" align="center">
        <td>{{$category->id}}</td>
        <td>{{$category->name}}</td>
        <td>{{$category->slug}}</td>
        <td class="center"><i class="fa fa-pencil fa-fw btn-edit" data-target="#editCategoryModal" data-toggle="modal"
                              data-id="{{$category->id}}" data-action="{{route('categories.edit',$category->id)}}"></i>
        </td>
        <td class="center"><i class="fa fa-trash-o fa-fw btn-delete"
                              data-action="{{ route('categories.destroy',$category->id) }}"></i></td>
    </tr>
@endforeach
<tr>
    <td colspan="5">
        {{$categories->links()}}
    </td>
</tr>
