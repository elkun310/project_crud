<div class="modal-header">
    <h4 class="modal-title">Sửa danh mục #{{$category->id}}</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<form method="post" id="updateCategoryForm">
    @csrf
    <input type="hidden" name="hidden_id" class="form-control" value="{{ $category->id }}">
    <div id="form-result"></div>
    <div class="modal-body">
        <div class="form-group">
            <label for="">Tiêu đề</label>
            <input type="text" name="name" class="form-control" value="{{ $category->name }}">
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-info pull-right" id="btn-update-category"
                data-action="{!! route('categories.update',$category->id) !!}">Lưu lại
        </button>
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
    </div>
</form>
<!--end-add-category-->
