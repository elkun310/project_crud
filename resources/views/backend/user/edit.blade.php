<div class="modal-header">
    <h5 class="modal-title" id="titleLabel">Sửa nhân viên</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="{{ route('users.update',$user->id) }}" method="post" id="editUserForm" class="form-horizontal"
      enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <span id="form_result"></span>
    <div class="modal-body">
        <div class="form-group">
            <div class="row">
                <label for="name" class="control-label col-md-4">Họ tên</label>
                <input type="text" class="form-control col-md-8" id="name" name="name" value="{{$user->name}}"/>
            </div>

        </div>
        <div class="form-group">
            <div class="row">
                <label for="email" class="control-label col-md-4">Email</label>
                <input type="email" class="form-control col-md-8" id="email" name="email" value="{{$user->email}}" autocomplete="current-email" />
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="image" class="control-label col-md-4">Ảnh đại diện</label>
                <div class="col-md-8">
                    <input type="file" id="image" name="image"/>
                    <img src="images/users/@if($user->image!=""){{$user->image}}@else{{"user.png"}}@endif"
                         id="avt_image" class="img-thumbnail"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="password" class="control-label col-md-4">Password</label>
                <input type="password" class="form-control col-md-8" id="password" name="password" autocomplete="current-password" />
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="address" class="control-label col-md-4">Địa chỉ</label>
                <input type="text" class="form-control col-md-8" id="address" name="address"
                       value="{{$user->address}}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="phone" class="control-label col-md-4">Số điện thoại</label>
                <input type="text" class="form-control col-md-8" id="phone" name="phone" value="{{$user->phone}}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="role" class="control-label col-md-4">Chức vụ</label>
                @foreach($roles as $role)
                    <label style="margin-right: 12px">
                        <input type="checkbox" value="{{ $role->id }}"
                               @foreach($roleByUser as $item)
                               @if($role->id == $item)
                               {{"checked"}}
                               @endif
                               @endforeach
                               name="role[]" id="role_{{$role->id}}"
                               style="vertical-align: middle;margin-right: 2px"/>{{ $role->name }}
                    </label>
                @endforeach
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="hidden_id" value="{{$user->id}}">
        <input type="submit" name="btn_update_user" id="btn_update_user" class="btn btn-success"
               value="Lưu lại" data-action="{{ route('users.update',$user->id) }}"/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
    </div>
</form>


