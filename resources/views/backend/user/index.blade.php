@extends('backend.master.master')
@section('title','User Management')
@section('content')
    <div class="container">
        <br/>
        <div align="right">
            <button type="button" name="add_user" id="add_user" class="btn btn-success" data-toggle="modal"
                    data-target="#userModal">
                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                Thêm
            </button>
        </div>
        <div class="row">
            <form data-action="{{ route('users.fetch_data') }}" method="get" id="fetchDataForm"
                  style="width: 100%">
                <input class="col-md-4" type="text" name="txt_search" id="txt_search"
                       style="float: right;border-radius: 5px;padding: 5px;" placeholder="Tìm kiếm....">
            </form>

        </div>
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">Danh sách nhân viên</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="user_table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Họ tên</th>
                            <th>Avatar</th>
                            <th>Email</th>
                            <th>Địa chỉ</th>
                            <th>Số điện thoại</th>
                            <th>Chức vụ</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @include('backend.user.data_body')
                        </tbody>
                    </table>
                    <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
                </div>
                <br/>
                <br/>
            </div>
        </div>

        <!-- Add User Modal -->
        <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="titleLabel">Thêm nhân viên</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="post" id="userForm" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <span id="form_result"></span>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="control-label col-md-4">Họ tên</label>
                                    <input type="text" class="form-control col-md-8" id="name" name="name"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="email" class="control-label col-md-4">Email</label>
                                    <input type="email" class="form-control col-md-8" id="email" name="email" autocomplete="current-email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="image" class="control-label col-md-4">Ảnh đại diện</label>
                                    <div class="col-md-8">
                                        <input type="file" id="image" name="image"/>
                                        <img src="images/users/user.png" id="avt_image" class="img-thumbnail"/>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="password" class="control-label col-md-4">Password</label>
                                    <input type="password" class="form-control col-md-8" id="password" name="password" autocomplete="current-password" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="address" class="control-label col-md-4">Địa chỉ</label>
                                    <input type="text" class="form-control col-md-8" id="address" name="address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="phone" class="control-label col-md-4">Số điện thoại</label>
                                    <input type="text" class="form-control col-md-8" id="phone" name="phone"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="role" class="control-label col-md-4">Chức vụ</label>
                                    @foreach($roles as $role)
                                        <label style="margin-right: 12px"><input type="checkbox" value="{{ $role->id }}"
                                                                                 name="role[]" id="role_{{$role->id}}"
                                                                                 style="vertical-align: middle;margin-right: 2px"/>{{ $role->name }}
                                        </label>
                                    @endforeach
                                    <br/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" name="btn_add_user" id="btn_add_user" class="btn btn-success"
                                   value="Lưu lại" data-action="{{route('users.store')}}"/>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--Edit Modal-->
        <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @parent
    <script src="js/backend/ajax/user.js"></script>
@endsection

