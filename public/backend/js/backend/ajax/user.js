$(document).ready(function () {
    //add user
    $(document).on('click', '#add_user', function () {
        $("#form_result").html('');
        $('#userForm')[0].reset();
    });

    //show edit user
    $(document).on('click', '.edit', function () {
        let url = $(this).data('action');
        $.ajax({
            url: url,
        })
            .done((data) => {
                $('#editUserModal .modal-content').html(data);
            })
    })

    //submit add
    $(document).on('click', '#btn_add_user', function (e) {
        e.preventDefault();
        let url = $(this).data('action');
        submitFormByAjax(url, '#user_table', '#userForm');
    })

    //submit update
    $(document).on('click', '#btn_update_user', function (e) {
        e.preventDefault();
        let url = $(this).data('action');
        submitFormByAjax(url, '#user_table', '#editUserForm');
    })

    $(document).on('click', '.delete', function () {
        let url = $(this).data('action');
        deleteRecord(url, '#user');
    });

    searchRecord('#user_table');

    paginate('#user_table');

});
