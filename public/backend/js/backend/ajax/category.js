$(function () {
    $(document).on('click', '.btn-show-add', function () {
        $('#addCategoryForm').trigger("reset");
    })

    paginate('categories', '#category_table');

    $(document).on('click', '#btn-add-category', function (e) {
        e.preventDefault();
        let url = $(this).data('action');
        submitFormByAjax(url, '#category_table', '#addCategoryForm');
    })

    $(document).on('click', '.btn-edit', function () {
        let url = $(this).data('action');
        $.ajax({
            url: url,
        })
            .done((response) => {
                $('#editCategoryModal .modal-content').html(response);
            })
            .fail((response) => {
                console.log(response);
            })
    })

    $(document).on('click', '#btn-update-category', function (e) {
        e.preventDefault();
        let url = $(this).data('action');
        updateFormByAjax(url, '#category_table', '#updateCategoryForm');
    })

    $(document).on('click', '.btn-delete', function (e) {
        e.preventDefault();
        let url = $(this).data('action');
        deleteRecord(url, '#category');
    })
});
